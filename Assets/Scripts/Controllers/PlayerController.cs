﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Your solution must make use of the following fields. If these values are changed, even at runtime,
    /// the character controller should respect the new values and act as detailed in the Unity inspector.
    /// </summary>

    [SerializeField]
    private float m_jumpApexHeight;

    [SerializeField]
    private float m_jumpApexTime;

    [SerializeField]
    private float m_terminalVelocity;

    [SerializeField]
    private float m_coyoteTime;

    [SerializeField]
    private float m_jumpBufferTime;

    [SerializeField]
    private float m_accelerationTimeFromRest;

    [SerializeField]
    private float m_decelerationTimeToRest;

    [SerializeField]
    private float m_maxHorizontalSpeed;

    [SerializeField]
    private float m_accelerationTimeFromQuickturn;

    [SerializeField]
    private float m_decelerationTimeFromQuickturn;

    public enum FacingDirection { Left, Right }

    //-------------------------
    //private CharacterGraphicsController gController;
    private Rigidbody2D RB;
    private FacingDirection LastDir;
    private float m_currentSpeed;
    Vector2 input;
    float sign = 0;
    private float HorMove;
    private float LastMove;
    //private bool jump = false;
    //Input.GetButtonDown(BUTTON_JUMP)
    bool readyToJump;
    float jumpMaxSpeed;
    float jumpSpeed;
    float jumpSpeedHolder;
    float acceleration;
    float fallingAcceleration;
    //float groundRayDistance = 0.02f;


    [SerializeField]
    private float gravity;

    public bool IsWalking()
    {
        // If the character is walking, then return the bool as true
        // .x only returns 1, 0 or -1
        // IsGrounded is not needed because we're only checking for horizontal movement
        if ((ShovelKnightInput.GetDirectionalInput().x != 0))
        {
            return true;
        }

        else
        {
            return false;
        }
    }

    public bool IsGrounded()
    {
        //                                  (vector2 origin, vector2 size, float angle, vector2 direction, float distance, int LayerMask)
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(0.19f, 0.19f), 0, Vector2.down, RB.velocity.y * Time.deltaTime, LayerMask.GetMask("Ground"));
       // RaycastHit2D hit = ((Physics2D.Raycast(transform.position + new Vector3(0.5f, 0), Vector2.down, groundRayDistance, layerMask: 1 << 9) ||
                            // Physics2D.Raycast(transform.position - new Vector3(0.5f, 0), Vector2.down, groundRayDistance, layerMask: 1 << 9)) && !readyToJump);

        if (hit.collider != null)
        {
            //RB.position = new Vector2(RB.position.x, hit.point.y + 0.2f);
            fallingAcceleration = 0;

            readyToJump = false;

            jumpSpeed = 0;

            return true;
        }
        else
        {
            return false;
        }
        //return false;
    }



    //public bool IsJumping()
    //{
    //    if (ShovelKnightInput.IsJumpPressed())
    //    {
    //        print("a");
    //        return true;
    //    }
    //    else
    //    {
    //        return false;
    //    }
    //}

    //void CheckForJump()
    //{

    //}

    //    - create a function called like CheckForJump() or something
    // -> CheckForJump() should run in update as long as you are grounded and not currently jumping
    // -> it checks if you press jump. if they do and they can jump, set a global bool called isCurrentlyJumping to true
    //- create a function called like DoJump() or something
    // -> have it run in Update when isCurrentlyJumping is true
    // -> do all your math here
    // -> once the player hits the apex height, set isCurrentlyJumping to false
    //public bool IsJumping()
    //{
    //    if ((ShovelKnightInput.IsJumpPressed().y != 0))
    //    {
    //        print("a");
    //        return true;
    //    }

    //    else
    //    {
    //        return false;
    //    }

    //    //else
    //    //{
    //    //    return false;
    //    //}
    //    //print("a");
    //    //return Input.GetButtonDown(BUTTON_JUMP);
    //}

    void CheckForGround()
    {
        //                                  (vector2 origin,     vector2 size, float angle, vector2 direction, float distance,       int LayerMask)
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, new Vector2(1, 1), 0, Vector2.down, RB.velocity.y * Time.deltaTime, LayerMask.GetMask("Ground"));
        if (hit.collider != null)
        {
            //print("a");
            RB.position = new Vector2(RB.position.x, hit.point.y + 0.2f);
        }
    }

    public FacingDirection GetFacingDirection()
    {
        input = ShovelKnightInput.GetDirectionalInput();

        // Makes sure SK stays facing the direction he was heading in
        if (input.x > 0)
        {
            LastDir = FacingDirection.Right;
            return FacingDirection.Right;
        }

        if (input.x < 0)
        {
            LastDir = FacingDirection.Left;
            return FacingDirection.Left;
        }

        return LastDir;
    }

    // Add additional methods such as Start, Update, FixedUpdate, or whatever else you think is necessary, below.

    void Start()
    {
        RB = GetComponent<Rigidbody2D>();
        jumpSpeedHolder = jumpSpeed;
        jumpMaxSpeed = m_jumpApexHeight / m_jumpApexTime;
    }

    void Update()
    {
        if (!IsGrounded() && !readyToJump)
        {
            FallDown();
        }

        if (ShovelKnightInput.WasJumpPressed() && IsGrounded())
        {
            Jump();
        }

        if (readyToJump)
        {
            // acc = jumpMaxSpeed / jumpDecelerationTime;

            acceleration = 2 * (m_jumpApexHeight) / (m_jumpApexTime * m_jumpApexTime);

            jumpSpeed = Mathf.Clamp(jumpSpeed - acceleration * Time.deltaTime, 0, jumpMaxSpeed);

            transform.Translate(Vector2.up * jumpSpeed * Time.deltaTime, Space.World);

            if (jumpSpeed == 0)
            {
                readyToJump = false;
            }
            //print(IsGrounded());
            if (!IsGrounded())
            {
                CheckForGround();
            }
        }
        // Checks for input in the direction of a vector
        // Before it was only getting input for the right, now it gets it in both ways
        input = ShovelKnightInput.GetDirectionalInput();
        //jump = ShovelKnightInput.IsJumpPressed();
        //print(jump);
        Vector2 newMovement = new Vector2();

        float newVal = 0;
        //m_currentSpeed = 0;

        //print(m_currentSpeed);
        if (IsWalking())
        {
            // acceleration is equal to the maximum speed SK can move divided by how fast he can move at the start
            float acceleration = m_maxHorizontalSpeed / m_accelerationTimeFromRest;
            // newVal is equal to 0, and that value is added to by what acceleration is
            newVal += acceleration * Time.deltaTime;
            newVal *= input.x;
            //print("A " + newVal);
            if (input.x > 0)
                m_currentSpeed = Mathf.Clamp(m_currentSpeed + newVal, 0, m_maxHorizontalSpeed);
            else if (input.x < 0)
                m_currentSpeed = Mathf.Clamp(m_currentSpeed + newVal, -m_maxHorizontalSpeed, 0);
        }

        else if (!IsWalking() && Mathf.Abs(RB.velocity.x) > 0)
        {
            float currentVeloX = Mathf.Abs(RB.velocity.x);
            float decceleration = m_maxHorizontalSpeed / m_decelerationTimeToRest;
            newVal = currentVeloX - decceleration * Time.deltaTime;
            //print("B " + newVal);
            m_currentSpeed = Mathf.Clamp(newVal, 0, m_maxHorizontalSpeed);
            m_currentSpeed *= Mathf.Sign(RB.velocity.x);
        }
        newMovement.x = m_currentSpeed;

        if (!IsGrounded())
        {
            newMovement.y = gravity;
        }
        //print(input + " " + m_currentSpeed);
        RB.velocity = newMovement;
    }

    void Jump()
    {
        jumpSpeed = 2 * (m_jumpApexHeight) / (m_jumpApexTime);
        readyToJump = true;
    }

    void FallDown()
    {
        transform.Translate(Vector2.down * fallingAcceleration * Time.deltaTime);

        fallingAcceleration += 0.5f;
    }


    void HorizontalMovement()
    {
        HorMove = ShovelKnightInput.GetDirectionalInput().x;

        //if (quickTurning) return;

        // If moving, accelerate to max speed
        // He is accelerating much too slowly
        if (HorMove != 0)
        {
            m_currentSpeed = m_currentSpeed + (m_maxHorizontalSpeed / m_accelerationTimeFromRest) * Time.deltaTime;
            LastMove = HorMove;
        }

        // If input is let go, deccelerate to 0
        // He is stopping much too slowly
        else
        {
            m_currentSpeed = m_currentSpeed - (m_maxHorizontalSpeed / m_accelerationTimeFromRest) * Time.deltaTime;
            HorMove = LastMove;
        }

        // If in the air, horizontal speed is limited
        // I set my velocity at the end of horizontal movement.  Update calls horizontal movement
        //(0,              0,)
        m_currentSpeed = Mathf.Clamp(m_currentSpeed, 0, m_maxHorizontalSpeed);

        RB.velocity = new Vector2(m_currentSpeed * ShovelKnightInput.GetDirectionalInput().x, RB.velocity.y);
    }

    //float horizontal;
    //float vertical;
    //float acceleration;
    //float decceleration;
    //input = ShovelKnightInput.GetDirectionalInput();

    //if (!IsGrounded())
    //    RB.velocity += new Vector2(0, -0.15f);

    /*if (Mathf.Sign(input.x) != 0)
    {
        sign = Mathf.Sign(input.x);
    }*/

    /*if (IsWalking())
    {

        // Because when you have current speed, it's only positive rn
        // I multiply my current speed by my input's x value before applying the velocity
        // RB.velocity = m_curretSpeed * ShovelKnightInput.GetDirectionalInput().x;
        float acceleration = m_maxHorizontalSpeed / m_accelerationTimeFromRest;
        // Clamping between 0 and max horizontal speed
        // Can only move right
        m_currentSpeed = m_currentSpeed + acceleration * Time.deltaTime;
    }

    else
    {
        float decceleration = m_maxHorizontalSpeed / m_decelerationTimeToRest;
        m_currentSpeed = m_currentSpeed - decceleration * Time.deltaTime;
    }

    // Probably something to do with his speed.  
    // If it is only positive, he will only go right unless you multiply it by something that determines his direction/sign
    // it's at the end of a function for horizontal movement, which is called by update
    if (ShovelKnightInput.GetDirectionalInput().magnitude != 0)
    {
        //m_currentSpeed = m_currentSpeed + (m_maxHorizontalSpeed / m_accelerationTimeFromRest) * Time.deltaTime;
    }

    //m_currentSpeed = Mathf.Clamp(Mathf.Abs(m_currentSpeed), 0, m_maxHorizontalSpeed);
    // retain what direction what I'm going in
    m_currentSpeed = Mathf.Clamp(m_currentSpeed, 0, m_maxHorizontalSpeed) * input.x;
    print(input + " " + m_currentSpeed);
    //RB.velocity = new Vector2(m_currentSpeed*sign, 0);
    RB.velocity += new Vector2(m_currentSpeed, 0);
    //RB.velocity = new Vector2(m_currentSpeed, 0);

    //if (quickTurning) return;*/
}
