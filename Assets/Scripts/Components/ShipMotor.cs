﻿using UnityEngine;
using System.Collections;

public class ShipMotor : MonoBehaviour
{
    public float AccelerationTime = 1;
    public float DecelerationTime = 1;
    public float MaxSpeed = 1;

    public float Acceleration = 1;
    public float Deceleration = 1;
    public float Speed;
    public float timeX, timeY;
    Vector2 Direction;


    /// <summary>
    /// Move the ship using it's transform only based on the current input vector. Do not use rigid bodies.
    /// </summary>
    /// <param name="input">The input from the player. The possible range of values for x and y are from -1 to 1.</param>
    
    //public void Start()
    //{
    //    speed = Vector2.zero;
    //}

    public void HandleMovementInput( Vector2 input )
    {
        // If there is input for any direction, move
        if (input != Vector2.zero)
        {
            Acceleration = MaxSpeed / AccelerationTime;
            Speed = Mathf.Clamp(Speed + Acceleration * Time.deltaTime, 0, MaxSpeed);
            Direction = input;
        }

        // If there is NO input for any direction, Decelerate
        else
        {
            Acceleration = MaxSpeed / DecelerationTime;
            Speed = Mathf.Clamp(Speed - Acceleration * Time.deltaTime, 0, MaxSpeed);
        }

        transform.Translate(Direction * Speed * Time.deltaTime, Space.World);
    }  
}
